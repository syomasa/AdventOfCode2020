#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void handleInput(FILE *inputFile, char* data);
void parse(int *limits, char **key, char **password, char *content);

int main()
{
	FILE *fp = fopen("input.txt", "r");
	if(!fp)
	{
		printf("Failed to open file\n");
		return -1;
	}
	char *data = "yes";

	handleInput(fp, data);
	fclose(fp);
	return 0;
}

void handleInput(FILE *inputFile, char* data)
{
	char content[2000];
	int limit[2]; // [0] min, [1] max
	char *key;
	char *password;
	int valids = 0;

	while(fgets(content, 2000, inputFile) != NULL)
	{
		parse(limit, &key, &password, content);
		//printf("limiters[%d, %d, %s, %s]\n", limit[0], limit[1], key, password);

		// start looping checking if password fullfills the rules
		if((password[limit[0]-1] == key[0]) ^ (password[limit[1]-1] == key[0]))
		{
			valids++;
		}
		/*for(int j = 0; password[j] != '\0'; j++)
		{
			if(key[0] == password[j])
			{
				i++;
			}
		}*/
		/*if(i == limit[0] && i <= limit[1])
		{
			valids++;
		}*/
	}
	printf("There are %d valid passwords\n", valids);
}

void parse(int *limits, char **key, char **password, char *content)
{
	/* param limits: parsed numbers are sent here
	 * param key: parsed key is sent here
	 * param password: parsed password is sent here
	 * param content: content what is parsed
	 */

	char *token;
	int i = 0;
	token = strtok(content, "- :");
	while(token != NULL)
	{
		if(atoi(token) != 0)
		{
			limits[i] = atoi(token);
		}
		else
		{
			*key = token;
			*password = strtok(NULL, "- :\n");
			break;
		}
		i++;
		token = strtok(NULL, "- :");
	}
}
