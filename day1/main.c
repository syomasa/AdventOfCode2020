#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	FILE *fp;
	char content[2000];
	char strings[200][10];
	int size = sizeof(strings) / sizeof(*strings);
	printf("Size of strings is %d\n", size);
	int i = 0;

	fp = fopen("input.txt", "r");
	if(!fp)
	{
		printf("Error opening the file\n");
	}

	while(fgets(content, 2000, fp) != NULL)
	{
		strcpy(strings[i], content);
		i++;
	}
	fclose(fp);

	for(i = 0; i < size; i++)
	{
		int x = atoi(strings[i]);
		for(int j = 1; j < size; j++)
		{
			int y = atoi(strings[j]);
			if((x+y) >= 2020)
			{
				continue;
			}
			else
			{
				for(int k = 2; k < size; k++)
				{
					int z =  atoi(strings[k]);
					if((x+y+z) == 2020)
					{
						printf("Successful formula is: %d + %d + %d = 2020\n",
								x, y, z);
						printf("And when multiplied %d\n", x*y*z);
						return 0;
					}
				}
			}
		}
	}
	return -1;
}
